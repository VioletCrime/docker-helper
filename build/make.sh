#!/bin/bash

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"

set -ex

if [ -z "$1" ]; then
    echo "Usage: $PROGNAME < container to build >"
    exit 1
fi

docker build -t $1 $1
