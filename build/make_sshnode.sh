#!/bin/bash

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"

pushd .
cd $PROGDIR

if ! [ -e ../../../src ]; then
    echo "ERROR: This script must be run from the docker-helper submodule of the Kommodum project!"
    exit 1
fi

if [ -e sshnode/src ]; then
    rm -rf sshnode/src
fi
cp -r ../../../src sshnode/
./make.sh sshnode

popd
