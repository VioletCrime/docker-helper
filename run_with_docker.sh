#!/bin/bash

set -e

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"

usage() {
    cat <<- EOF
usage: $PROGNAME [-i image_name|--image image_name] <options>

Options:
    -h   Show this message
    -i   Specify the Docker image to run with (required).
    -p   Specify port(s) to open
    -g   Use Go environment
EOF
}

parse_args() {
    local OPTIND=1
    while getopts "hi:p:g" OPTION
    do
        case $OPTION in
            h)
                usage ; exit 0 ;;
            i)
                IMAGE=${OPTARG} ;;
            p)
                PORTS+="-p ${OPTARG}:${OPTARG} " ;;
            g)
                IS_GO=true
        esac
    done

    shift "$((OPTIND-1))"
    CMDS=$@

    if [[ -z $IMAGE ]]; then
        echo "ERROR: Image must be specified. Please see usage for details."
        exit 1
    fi
}

main() {
    local IMAGE PORTS
    IS_GO=false
    parse_args $ARGS

    # Check to see if we have a local build definition, run it if so
    if [ -e $PROGDIR/build/make_$IMAGE.sh ]; then
        bash $PROGDIR/build/make_$IMAGE.sh
    fi

    # Get the workspace to pass into the container
    WORKSPACE=`git rev-parse --show-toplevel`/..  # Find top level directory for this git repo
    WORKSPACE=`readlink -f $WORKSPACE`  # Evaluate the '/..' from above to get parent directory
    TARGET_WORKSPACE=/workspace
    if $IS_GO; then
        TARGET_WORKSPACE=/go/src
    fi
    SUBDIR=`echo $(pwd) | sed s@$WORKSPACE/@@`

    docker run -it -v $WORKSPACE:$TARGET_WORKSPACE $PORTS $IMAGE bash -c "cd $TARGET_WORKSPACE && $CMDS"
}

main
